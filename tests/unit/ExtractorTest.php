<?php


use extractor\Extractor;

class ExtractorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @return array
     */
    public function _getDataset(): array
    {
        return [
            [
                567.84,
                '41001588914079',
                '1430',
                'Пароль: 1430
Спишется 567,84р.
Перевод на счет 41001588914079',
            ],
            [
                1.01,
                '4100158847219',
                '6135',
                'Пароль: 6135
Спишется 1,01р.
Перевод на счет 4100158847219',
            ],
            [
                10000,
                '4100158847409',
                '03230',
                'Никому не говорите пароль! Его спрашивают только мошенники.
Пароль: 03230
Перевод на счет 4100158847409
Вы потратите 10000р.',
            ],
        ];
    }

    /**
     * @param string $sum
     * @param string $receiver
     * @param string $smsCode
     * @param string $response
     * @dataProvider _getDataset
     */
    public function test($sum, $receiver, $smsCode, $response)
    {
        $extractor = new Extractor();
        $result = $extractor->extract($response);

        $this->tester->assertEquals($sum, $result->getPaymentAmount());
        $this->tester->assertEquals($receiver, $result->getReceiver());
        $this->tester->assertEquals($smsCode, $result->getSmsCode());
    }
}

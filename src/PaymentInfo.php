<?php


namespace extractor;


class PaymentInfo
{
    /** @var string */
    protected $smsCode;
    /** @var float */
    protected $paymentAmount;
    /** @var string */
    protected $receiver;

    /**
     * ExtractResult constructor.
     * @param string $smsCode
     * @param float  $paymentAmount
     * @param string $receiver
     */
    public function __construct($smsCode, $paymentAmount, $receiver)
    {
        $this->smsCode = $smsCode;
        $this->paymentAmount = $paymentAmount;
        $this->receiver = $receiver;
    }

    /**
     * @return string
     */
    public function getSmsCode()
    {
        return $this->smsCode;
    }

    /**
     * @return float
     */
    public function getPaymentAmount()
    {
        return $this->paymentAmount;
    }

    /**
     * @return string
     */
    public function getReceiver()
    {
        return $this->receiver;
    }
}

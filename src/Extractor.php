<?php


namespace extractor;


use RuntimeException;

class Extractor
{
    public function extract(string $data): PaymentInfo
    {
        $receiver = $this->ejectWalletId($data);
        $data = str_replace($receiver, '', $data);
        $sum = $this->ejectSum($data);
        $data = str_replace($sum, '', $data);
        $code = $this->ejectSmsCode($data);

        return new PaymentInfo($code, $sum, $receiver);
    }

    protected function ejectWalletId(string $data): string
    {
        try {
            return $this->matchOnlyOneByRegex('/41001\d{8,11}/', $data);
        } catch (RuntimeException $e) {
            throw new RuntimeException('Unable to detect wallet id on: ' . $data);
        }
    }

    protected function ejectSum(string $data): float
    {
        try {
            $sum = $this->matchOnlyOneByRegex('/\d+((р|\sр)|(,|\.)\d{2}(р|\sр))/', $data);
        } catch (RuntimeException $e) {
            throw new RuntimeException('Unable to sum of payment on: ' . $data);
        }

        $sum = trim(mb_substr($sum, 0, mb_strlen($sum) - 1));
        $sum = str_replace(',', '.', $sum);

        return (float) $sum;
    }

    protected function ejectSmsCode(string $data): string
    {
        try {
            $code = $this->matchOnlyOneByRegex('/\s\d{4,6}/', $data);
        } catch (RuntimeException $e) {
            throw new RuntimeException('Unable to detect sms code');
        }

        return mb_substr($code, 1, mb_strlen($code));
    }

    protected function matchOnlyOneByRegex(string $regex, string $resource): string
    {
        preg_match($regex, $resource, $matches, PREG_OFFSET_CAPTURE, 0);

        if (count($matches) < 1) {
            throw new RuntimeException('Unable to match on: ' . $resource);
        }

        return $matches[0][0];
    }
}
